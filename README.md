# Master_Thesis_Hanna_Sahlstrom

Gitlab repository for Master thesis of Hanna Sahlstrom, written 2020-2021 at the Norwegian University of Life Sciences (NMBU).

The R code used in this thesis can be found in the "R_code" directory.

The input files used for the data analysis can be found in the "Data" directory.

The output files, plots, statistical analysis outputs, and alignments can be found in the "Output_data" directory.

Any unix executables used in the data analysis can be found in the "Tools" directory.